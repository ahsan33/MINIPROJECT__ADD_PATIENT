<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \Ahsan\BITM\SEIP117343\Patient;
use \Ahsan\BITM\SEIP117343\Message;

$patient = new Patient();
$add = $patient->index();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>Patient</title>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <!-- Bootstrap -->
                <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                    </head>

                    <body>
                        <div>
                            <?php
                            if ((array_key_exists('Message', $_SESSION)) && !empty($_SESSION['Message'])) {
                                echo Message::flash();
                            }
                            ?>
                        </div>
                        <div class="container">
                            <table class="table table-bordered" class="table table-responsive" style="width:100%">
                                <br>  <thead>
                                        <tr>
                                            <td><b>Sl.</b></td>
                                            <td><b>Patient name</b></td>		
                                            <td><b>Gender</b></td>
                                            <td><b>Address</b></td>
                                            <td><b>Cell phone</b></td>
                                            <td colspan="3"><center><b>Action</b></center></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sl = 0;
                                        foreach ($add as $patient):
                                            $sl++;
                                            ?>
                                            <tr>
                                                <td><?php echo $sl; ?></td>
                                                <td><a href="show.php?id=<?php echo $patient['id']; ?>"><?php echo $patient['title']; ?></a></td>		
                                                <td><?php echo $patient['gender']; ?></td>
                                                <td><?php echo $patient['address']; ?></td>
                                                <td><?php echo $patient['phone']; ?></td>

                                                <td colspan="3"><center>
                                                        <button type="submit" class="btn btn-success"><a href="edit.php?id=<?php echo $patient['id']; ?>">Edit</a></button>
                                                        <button type="submit" class="btn btn-danger"><a href="delete.php?id=<?php echo $patient['id']; ?>">Delete</a></button></center></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>
                                    </tbody>
                                    <tfoot><div><center><button type="button" class="btn btn-info"><a href="create.php">Add New</a></button> 
                                            <button type="button" class="btn btn-info"><a href="01simple-download-xlsx.php">Download as XL</a></button>
                                            <button type="button" class="btn btn-info"><a href="pdf.php">Download as PDF</a></button></center></div></tfoot><br>
                                        </table>
                                        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                                        <!-- Include all compiled plugins (below), or include individual files as needed -->
                                        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
                                        </div>
                                        </body>
                                        </html>