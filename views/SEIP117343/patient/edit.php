<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \Ahsan\BITM\SEIP117343\Patient;
use \Ahsan\BITM\SEIP117343\Message;
use \Ahsan\BITM\SEIP117343\Utility;

$patients = new Patient();
//Utility::dd($students);
$patient = $patients->edit($_GET['id']);
//Utility::dd($student);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Edit</title>

        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body><div class="container"><br>
            <form action="update.php" method="post" class="form-inline">
                <div class="form-group col-md-10">
                    
                    <input type="hidden" class="form-control" 
                           name="id"
                           id="id"
                           value="<?php echo $patient->id; ?>">
                    
                    <label for="title">Patient name:</label>
                    <input type="text" name="title" class="form-control"
                           value="<?php echo $patient->title; ?>"><br><br>
                    
                    <label for="gender">Gender:</label>
                    <label><input type="radio" name="gender" id="male" value="Male" <?php if(preg_match('/Male/', $patient->gender)){ echo "checked";} ?>> Male</label>
                    <label><input type="radio" name="gender" id="female" value="Female"<?php if(preg_match('/Female/', $patient->gender)){ echo "checked";} ?>> Female</label>
                    <?php  $patient->gender;?><br><br>
                           
                   <label for="address">Address:</label>
                   <textarea name="address" class="form-control" rows="3">
                              <?php echo $patient->address; ?></textarea><br><br>
                   
                   
                    <label for="phone">Mobile No.:</label>
                    <input type="text" name="phone" class="form-control"
                           value="<?php echo $patient->phone; ?>"><br><br>

                    <button type="submit" class="btn btn-primary">Update</button><br><br>
                </div>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
