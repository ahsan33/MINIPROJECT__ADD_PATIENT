<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \Ahsan\BITM\SEIP117343\Patient;
use \Ahsan\BITM\SEIP117343\Message;

$patient = new Patient();
$add = $patient->index();
$trs="";
?>


    <?php
    $sl = 0;
    foreach ($add as $patient):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>" .$sl."</td>";
    $trs.="<td>" .$patient['title']."</td>";
    $trs.="<td>" .$patient['gender']."</td>";
    $trs.="<td>" .$patient['address']."</td>";
    $trs.="<td>" .$patient['phone']."</td>";
    $trs.="</tr>";
    endforeach;
?>

<?php
$html = <<<AHSAN
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>Student Registration</title>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
                <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
              </head>

                    <body>
                      
                        <div class="container">
                            <table class="table table-bordered" class="table table-responsive" style="width:100%">
                                <br>  <thead>
                                        <tr>
                                            <td><b>Sl.</b></td>
                                            <td><b>Patient name</b></td>		
                                            <td><b>Gender</b></td>
                                            <td><b>Address</b></td>
                                            <td><b>Cell phone</b></td>
                                         </tr>
                                    </thead>
                                    <tbody>
                                    echo $trs;
                                    </tbody>
                                    </table>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                                        <!-- Include all compiled plugins (below), or include individual files as needed -->
                                        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
                                        
                                        </div>
                                        </body>
                                        </html>

AHSAN;
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "StudentRegistration" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "mpdf" . DIRECTORY_SEPARATOR . "mpdf" . DIRECTORY_SEPARATOR . 'mpdf.php';
$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;



