<?php

namespace Ahsan\BITM\SEIP117343;
use \Ahsan\BITM\SEIP117343\Utility;
use \Ahsan\BITM\SEIP117343\Message;

class Patient{

    public $id = "";
    public $title = "";
    public $gender = "";
    public $address = "";
    public $phone = "";
   

    public function __construct() {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("addpatient") or die("Cannot select database.");
    }

        public function index() {
        //Utility::dd($_REQUEST);
        $patient = array();
        $query = "SELECT * FROM `patient`";
        
        $result = mysql_query($query);
        //Utility::dd($result);
        while ($row = mysql_fetch_assoc($result)) {
            $patient[] = $row;
            //Utility::dd($row);
        }
        return $patient;
    }
    
    public function store() {
        $title = $_REQUEST['title'];
        $gender = $_REQUEST['gender'];
        $address = $_REQUEST['address'];
        $phone = $_REQUEST['phone'];
     
        $query = "INSERT INTO `addpatient`.`patient` (`title`, `gender`, `address`, `phone`) VALUES ('{$this->title}', '{$this->gender}', '{$this->address}', '{$this->phone}');";
        //Utility::dd($query);
        if (mysql_query($query)) {
            Message::set('Patient data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }
    
        public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "SELECT * FROM `patient` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }
    
   
        public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "SELECT * FROM `patient` WHERE id=" . $id;
        $b = mysql_query($query);
        //Utility::dd($result);
        $result = mysql_fetch_object($b);
        //Utility::dd($result);
        return $result;
    }

        public function update(){

        $query = "UPDATE `patient` SET `title` = '".$this->title."', `gender` = '".$this->gender."', `address` = '".$this->address."', `phone` = '".$this->phone."' WHERE `patient`.`id` = ".$this->id;

        if (mysql_query($query)) {
            Message::set('Patient data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }
    
       public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "DELETE FROM `addpatient`.`patient` WHERE `patient`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Patient data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }
    
    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('title', $data)) {

            $this->title = $data['title'];
            $this->gender = $data['gender'];
            $this->address = $data['address'];
            $this->phone = $data['phone'];
 
            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
